package Challenge59

import (
	"crypto/hmac"
	"errors"
	"fmt"
	"math/big"
	"crypto/sha256"
    "gitlab.com/cvolyz/cryptopals_golang/DH"
    "gitlab.com/cvolyz/cryptopals_golang/eliptic"
)





const (
	dhKeyAgreementConst = "crazy flamboyant for the rap enjoyment"
)

func MAC(k []byte) []byte {
	mac := hmac.New(sha256.New, k)
	mac.Write([]byte(dhKeyAgreementConst))
	return mac.Sum(nil)
}


func Factorize2(n *big.Int, upperBound *big.Int) []*big.Int {
	factors := make([]*big.Int, 0)

	i := new(big.Int).Set(DH.Big2)
	tmp := new(big.Int)
	newN := new(big.Int).Set(n)

	for {
		tmp.Mod(newN, i)

		if tmp.Cmp(DH.Big0) == 0 {
			factors = append(factors, new(big.Int).Set(i))
			for tmp.Mod(newN, i).Cmp(DH.Big0) == 0 {
				newN.Set(tmp.Div(newN, i))
			}
		}

		if newN.Cmp(DH.Big1) == 0 {
			break
		}

		if i.Cmp(upperBound) >= 0 {
			break
		}

		i.Add(i, DH.Big1)
	}

	return factors
}



func ChineseRemainderTheorem(a, n []*big.Int) (*big.Int, *big.Int, error) {
	p := new(big.Int).Set(n[0])
	for _, n1 := range n[1:] {
		p.Mul(p, n1)
	}
	var x, q, s, z big.Int
	for i, n1 := range n {
		q.Div(p, n1)
		z.GCD(nil, &s, n1, &q)
		if z.Cmp(big.NewInt(1)) != 0 {
			return nil, p, fmt.Errorf("%d not coprime", n1)
		}
		x.Add(&x, s.Mul(a[i], s.Mul(&s, &q)))
	}
	return x.Mod(&x, p), p, nil
}



// pickRandomPoint picks a random point on given curve
func pickRandomPoint(curve elliptic.Curve, order *big.Int) (x *big.Int, y *big.Int) {
	k := new(big.Int).Div(curve.Params().N, order).Bytes()

	for {
		x, y = elliptic.GeneratePoint(curve)
		x, y = curve.ScalarMult(x, y, k)

		if x.Cmp(DH.Big0) == 0 && y.Cmp(DH.Big0) == 0 {
			continue
		}

		return
	}
}

// ecdh performs DH with given curve, public and private keys
func ecdh(curve elliptic.Curve, x *big.Int, y *big.Int, privateKey []byte) []byte {
	ssx, ssy := curve.ScalarMult(x, y, privateKey)
	return MAC(elliptic.Marshal(curve, ssx, ssy))
}

// checkDuplicate returns true if no duplicates were found
func checkDuplicate(reminders []*big.Int, modules []*big.Int, r *big.Int, m *big.Int) bool {
	if len(reminders) != len(modules) {
		panic("checkDuplicate: len(reminders) != len(modules)")
	}

	ok := true

	for i := 0; i < len(reminders); i++ {
		if reminders[i].Cmp(r) == 0 && modules[i].Cmp(m) == 0 {
			ok = false
			break
		}
	}

	return ok
}

func InvalidCurveAttack(oracleECDH func(x, y *big.Int) []byte) (*big.Int, error) {
	invalidCurves := []elliptic.Curve{elliptic.P128V1(), elliptic.P128V2(), elliptic.P128V3()}

	var modules, remainders []*big.Int

	for _, curve := range invalidCurves {
		factors := Factorize2(curve.Params().N, big.NewInt(1<<16))
		if len(factors) == 0 {
			return nil, errors.New("factors not found")
		}
		if factors[0].Cmp(DH.Big2) == 0 {
			factors = factors[1:]
		}

		for _, factor := range factors {
			x, y := pickRandomPoint(curve, factor)

			ss := oracleECDH(x, y)

			for k := big.NewInt(1); k.Cmp(factor) <= 0; k.Add(k, DH.Big1) {
				ss1 := ecdh(curve, x, y, k.Bytes())

				if hmac.Equal(ss, ss1) && checkDuplicate(remainders, modules, k, factor) {
					remainders = append(remainders, new(big.Int).Set(k))
					modules = append(modules, factor)
					break
				}
			}
		}
	}

	x, _, err := ChineseRemainderTheorem(remainders, modules)
	if err != nil {
		return nil, fmt.Errorf("chinese remainder theorem: %s", err.Error())
	}

	return x, nil
}
