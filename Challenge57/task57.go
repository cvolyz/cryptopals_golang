package Challenge57

import (
    "fmt"
    "math"
    "math/big"
    "crypto/rand"
    "github.com/ghhenry/intfact"
    "gitlab.com/cvolyz/cryptopals_golang/DH"
    "crypto/sha1"
    
)


		

type Factor struct {
	Fac *big.Int
	Exp  int64
}

type Chinese_theorem struct {
	bi *big.Int
	ri *big.Int
}

func Factorize(n *big.Int) []Factor {
	factors := make([]Factor, 0)
	l := intfact.NewFactors(n)
	l.TrialDivision(math.MaxUint32)
	for p := l.First; p != nil; p = p.Next {
		factors = append(factors, Factor{
			p.Fac, int64(p.Exp),
		})
	}
	return factors
}



func MAC(K *big.Int,m string) []byte{
    var b = 64
    var ipad = ""
    var opad = ""
    var zero = ""
    var key string
    for i:=0;i<b;i++{
        ipad = ipad + "54"
        opad = opad + "92"
        if i<44{
            zero = zero + "0"
        }
    }
//     fmt.Println(ipad)
    var s0 []byte = K.Bytes()
    if len(s0) != b{
        q := sha1.Sum(s0)
        key = string(q[:]) + zero
    }else {
        key = string(K.Bytes())
    }
    
    ikeypad := DH.Xor([]byte(key),[]byte(ipad))
    k_ipad := string(ikeypad) + m
    q := sha1.Sum([]byte(k_ipad))
    okeypad := string(DH.Xor([]byte(key),[]byte(opad))) + string(q[:])
    q = sha1.Sum([]byte(okeypad))
    
    return q[:]
}




func Subgroup_confinement_attacks (p, q *big.Int, alice, bob DH.DiffieHellman) *big.Int{
    pminus1 := new(big.Int).Sub(p, DH.Big1)
//        j := new(big.Int).Div(pminus1,q)
    var gotFactors = []Factor{
		{big.NewInt(2), 1},
		{big.NewInt(3), 2},
		{big.NewInt(5), 1},
		{big.NewInt(109), 1},
		{big.NewInt(7963), 1},
		{big.NewInt(8539), 1},
		{big.NewInt(20641), 1},
		{big.NewInt(38833), 1},
		{big.NewInt(39341), 1},
		{big.NewInt(46337), 1},
		{big.NewInt(51977), 1},
		{big.NewInt(54319), 1},
		{big.NewInt(57529), 1},
		{big.NewInt(96142199), 1},
     }
    
//     gotFactors := factorize(j)
    m := "crazy flamboyant for the rap enjoyment"
    var h *big.Int
    flag1 := true
    var t []byte
    var ch_th = []Chinese_theorem{}
    //var bi = []factor{}
    //var ri = []factor{}
    var mul = DH.Big1
    
    for i:=0; i<len(gotFactors) && flag1 == true && mul.Cmp(q) != 1;  i++{
        var l *big.Int
        var r  = gotFactors[i].Fac
        var flag2 = false
        for flag2!=true{
            var random, error6 = rand.Int(rand.Reader, p)
            var degree = new(big.Int).Div(pminus1,r)
            h = new(big.Int).Exp(random, degree, p)
            
            if h.Cmp(DH.Big1) != 0 && error6 == nil{
                flag2 = true
            }
        }
        K := bob.Get_shared_secret_key(h)
        t = MAC(K,m)
        
//         z := new(big.Int).Add(r, big1)
        var key *big.Int
        for l=DH.Big2; l.Cmp(r) != 0; {
            
            key = new(big.Int).Exp(h, l, p)
            mac := MAC(key,m)
            
            if string(mac) == string(t) {
                ch_th = append(ch_th, Chinese_theorem{l,r})
                mul = new(big.Int).Mul(mul,r)
                break
            }

            l=new(big.Int).Add(l, DH.Big1)
            
        }
        
    }
    
    for i:=0; i<len(ch_th); i++ {
        fmt.Println("x = ", ch_th[i].bi, " mod " ,ch_th[i].ri)
    }
    
    var M0 = mul
    var mi = []Factor{}
    var yi = []Factor{}
    
    for i:=0; i<len(ch_th); i++ {
        mi = append(mi, Factor{new(big.Int).Div(M0,ch_th[i].ri),1})
    }
    

    for i:=0; i<len(mi); i++{
        e := new(big.Int).Sub(ch_th[i].ri, DH.Big2)
        yi = append(yi, Factor{new(big.Int).Exp(mi[i].Fac,e,ch_th[i].ri),1})
    }

    
    var x *big.Int = DH.Big0
    for i:=0; i<len(mi); i++{
        e := new(big.Int).Mul(mi[i].Fac,yi[i].Fac)
        d := new(big.Int).Mul(ch_th[i].bi, e)
        x = new(big.Int).Add(x,d)
    }
        

    x = new(big.Int).Exp(x, DH.Big1, M0)
    
    return x
}

    
    



