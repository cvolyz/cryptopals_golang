package Challenge59

import (
	"testing"
    "bytes"
    "math/big"
    "gitlab.com/cvolyz/cryptopals_golang/eliptic"
)

func NewECDHAttackOracle(curve elliptic.Curve) (
	ecdh func(x, y *big.Int) []byte,
	isKeyCorrect func([]byte) bool,
	getPublicKey func() (x, y *big.Int),
) {
	privateKey, x, y, err := elliptic.GenerateKey(curve, nil)
	if err != nil {
		panic(err)
	}

	ecdh = func(x, y *big.Int) []byte {
		sx, sy := curve.ScalarMult(x, y, privateKey)
		return MAC(elliptic.Marshal(curve, sx, sy))
	}

	isKeyCorrect = func(key []byte) bool {
		// skipping trailing zeros in fixed size big-endian byte representation of big.Int
		// e.g. if the original private key is 886092136281582889795402858978242928
		// then it's 16-byte representation will be [0 170 167 183 29 163 210 19 176 223 2 100 1 190 113 112]
		// but the given key in big-endian byte representation derived from big.Int doesn't have first zero:
		// [170 167 183 29 163 210 19 176 223 2 100 1 190 113 112]
		i := 0
		for i < len(privateKey) && privateKey[i] == 0 {
			i++
		}

		return bytes.Equal(privateKey[i:], key)
	}

	getPublicKey = func() (*big.Int, *big.Int) {
		return x, y
	}

	return
}


func TestECDHInvalidCurveAttack(t *testing.T) {
	p128 := elliptic.P128()

	oracle, isKeyCorrect, _ := NewECDHAttackOracle(p128)

	privateKey, err := InvalidCurveAttack(oracle)
	if err != nil {
		t.Fatalf("%s: %s\n", t.Name(), err.Error())
	}
	t.Logf("%s: Private key: %d\n", t.Name(), privateKey)

	if !isKeyCorrect(privateKey.Bytes()) {
		t.Fatalf("%s: wrong private key was found in the invalid curve attack\n", t.Name())
	}
} 
