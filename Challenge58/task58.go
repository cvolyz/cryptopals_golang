package Challenge58

import (
    "fmt"
    "crypto/hmac"
    "crypto/sha256"
    "math"
    "math/big"
    "crypto/rand"
    "gitlab.com/cvolyz/cryptopals_golang/DH"

)

func MAC(key []byte, msg []byte) []byte {
	mac := hmac.New(sha256.New, key)
	mac.Write(msg)
	return mac.Sum(nil)
}


func Factorize2(n *big.Int, upperBound *big.Int) []*big.Int {
	factors := make([]*big.Int, 0)

	i := new(big.Int).Set(DH.Big2)
	tmp := new(big.Int)
	newN := new(big.Int).Set(n)

	for {
		tmp.Mod(newN, i)

		if tmp.Cmp(DH.Big0) == 0 {
			factors = append(factors, new(big.Int).Set(i))
			for tmp.Mod(newN, i).Cmp(DH.Big0) == 0 {
				newN.Set(tmp.Div(newN, i))
			}
		}

		if newN.Cmp(DH.Big1) == 0 {
			break
		}

		if i.Cmp(upperBound) >= 0 {
			break
		}

		i.Add(i, DH.Big1)
	}

	return factors
}



func DHSmallSubgroupAttack(p, cofactor *big.Int, bob DH.DiffieHellman) (*big.Int, *big.Int){
    pminus1 := new(big.Int).Sub(p, DH.Big1)
   
    /*
    var gotFactors = []Challenge57.Factor{
        {big.NewInt(2), 1},
   		{big.NewInt(12457), 1},
   		{big.NewInt(14741), 1},
   		{big.NewInt(18061), 1},
   		{big.NewInt(31193), 1},
  		{big.NewInt(33941), 1},
   		{big.NewInt(63803), 1},

       }
*/
    
    gotFactors := Factorize2(cofactor,big.NewInt(1<<16))
    
    var h *big.Int
   
    
    var bi, ri []*big.Int
    
    
    for _, r := range gotFactors {
        
 
        degree := new(big.Int).Div(pminus1,r)
        
        h = new(big.Int).Set(DH.Big1)
        for h.Cmp(DH.Big1) == 0 {
            random, _ := rand.Int(rand.Reader, p)
        
        for random.Cmp(DH.Big0) == 0 {
				random, _ = rand.Int(rand.Reader, p)
			}
        
            
            h.Exp(random, degree, p)
            
        }
        
        
        K := bob.Get_shared_secret_key(h)
        t := MAC(K.Bytes(),[]byte("crazy flamboyant for the rap enjoyment"))
        

        
        var key *big.Int
        for l:=big.NewInt(1); l.Cmp(r) <= 0; l.Add(l, DH.Big1){
            key = new(big.Int).Exp(h, l, p)
            mac := MAC(key.Bytes(),[]byte("crazy flamboyant for the rap enjoyment"))
            
            
            
            if hmac.Equal(t, mac) {
                
                bi = append(bi, new(big.Int).Set(l))
                
                ri = append(ri, new(big.Int).Set(r))
                
                break
            }

        }
        
    }
    
    mul := new(big.Int).Set(ri[0])
	for _, n1 := range ri[1:] {
		mul.Mul(mul, n1)
        
	}
   
    var M0 = mul
    var mi, yi []*big.Int
    
    
    for i:=0; i<len(ri); i++ {
        mi = append(mi, new(big.Int).Div(M0,ri[i]))
    }
    
    
    for i:=0; i<len(mi); i++{
        e := new(big.Int).Sub(ri[i], DH.Big2)
        yi = append(yi, new(big.Int).Exp(mi[i],e,ri[i]))
    }

    
    var x *big.Int = DH.Big0
    for i:=0; i<len(mi); i++{
        e := new(big.Int).Mul(mi[i],yi[i])
        d := new(big.Int).Mul(bi[i], e)
        x = new(big.Int).Add(x,d)
    }
        
    x = new(big.Int).Exp(x, DH.Big1, M0)
    return x , M0
}


func f(y, k, p *big.Int) *big.Int {
    // f = 2^(y mod k) mod p
    return new(big.Int).Exp(DH.Big2, new(big.Int).Mod(y, k), p)
}


func calcN(p, k *big.Int) *big.Int {
	N := new(big.Int).Set(DH.Big0)

	for i := new(big.Int).Set(DH.Big0); i.Cmp(k) < 0; i.Add(i, DH.Big1) {
		N.Add(N, f(i, k, p))
	}

	
	
	N.Div(N, k)

	return N.Mul(big.NewInt(4), N)
}


func calcK(a, b *big.Int) *big.Int {
	// k = log2(sqrt(b-a)) + log2(log2(sqrt(b-a))) - 2
	sqrtba := math.Sqrt(float64(new(big.Int).Sub(b, a).Uint64()))
	logSqrt := math.Log2(sqrtba)
	logLogSqrt := math.Log2(logSqrt)
	return new(big.Int).SetUint64(uint64(logSqrt + logLogSqrt - 2))
}

func tameKangaroo(g, b, p, k *big.Int) (xT, yT *big.Int) {

    N := calcN(p, k)

    
	xT = new(big.Int).Set(DH.Big0)
	yT = new(big.Int).Exp(g, b, p)
    
	tmp := new(big.Int)

	for i := new(big.Int).Set(DH.Big0); i.Cmp(N) < 0; i.Add(i, DH.Big1) {
		// xT := xT + f(yT)
		xT.Add(xT, f(yT, k, p))
        
		// yT := yT * g^f(yT)
		yT.Mod(yT.Mul(yT, tmp.Exp(g, f(yT, k, p), p)), p)
        
	}

	return
}


func catchKangaroo(p, g, y, a, b *big.Int) *big.Int{


    k := calcK(a, b)
	xT, yT := tameKangaroo(g, b, p, k)
    
    tmp := new(big.Int)
    
    xW := new(big.Int).Set(DH.Big0)
    yW := new(big.Int).Set(y)
     
    
    
	// while xW < b - a + xT:
	for xW.Cmp(tmp.Add(tmp.Sub(b, a), xT)) < 0 {
		// xW := xW + f(yW)
		xW.Add(xW, f(yW, k, p))

		// yW := yW * g^f(yW)
		yW.Mod(yW.Mul(yW, tmp.Exp(g, f(yW, k, p), p)), p)

                //fmt.Println("yW = ", yW)
        //fmt.Println("yT = ", yT)
        
		// if yW = yT:
		if yW.Cmp(yT) == 0 {
			// b + xT - xW
			return tmp.Add(b, tmp.Sub(xT, xW))
		}
	}
    
    fmt.Println("No result!")        
    return nil
}


func DHKangarooAttack(p, g, q, cofactor *big.Int, bob DH.DiffieHellman) *big.Int{
    
    tmp := new(big.Int)
    
    // x = n mod r
    n , r := DHSmallSubgroupAttack(p, cofactor, bob)
    
    y := bob.Get_public_key()
    
    // y' = y * g^-n
	newY := new(big.Int).Mod(tmp.Mul(y, tmp.Exp(g, tmp.Neg(n), p)), p)

    // g' = g^r
	newG := new(big.Int).Exp(g, r, p)

    // [a, b] = [0, (q-1)/r]
	a := DH.Big0
	b := new(big.Int).Div(tmp.Sub(q, DH.Big1), r)

    	// if q too small
	if b.Cmp(DH.Big0) == 0 {
		b = new(big.Int).SetUint64(1 << 20)
	}
	
	m := catchKangaroo(p, newG, newY, a, b)
    
    // x = n + m*r
    x := new(big.Int).Add(n, tmp.Mul(m, r))
    
    return x
}


 
